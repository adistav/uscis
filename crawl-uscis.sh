#!/bin/bash

set -eu -o pipefail

BASEDIR="$(cd "`dirname $0`" && pwd)"
GET_USCIS="$BASEDIR/get-uscis.py"
DATADIR="$BASEDIR/data/`date -u +%Y-%m-%d`"

FORM="N-400"

mkdir "$DATADIR"
cd "$DATADIR"

wget -O- "https://egov.uscis.gov/processing-times/api/formoffices/${FORM}" | python -m json.tool >${FORM}

$GET_USCIS --list |
while read OFFICE
do
    wget -O- "https://egov.uscis.gov/processing-times/api/processingtime/${FORM}/${OFFICE}" | python -m json.tool >${OFFICE}
done
