#!/usr/bin/env python3

import argparse
import json
import os
from operator import itemgetter


def json_load_file(filename):
    with open(filename) as open_file:
        return json.load(open_file)


def extract_offices(formoffices):
    return formoffices['data']['form_offices']['offices']


def add_times(offices, directory):
    for office in offices:
        try:
            office_times = json_load_file(os.path.join(directory, office['office_code']))
        except FileNotFoundError:
            continue
        office_time_range = office_times['data']['processing_time']['range']
        upper_time, lower_time = (time['value'] for time in office_time_range)
        office.setdefault('lower_time', []).append(lower_time)
        office.setdefault('upper_time', []).append(upper_time)


def main():
    parser = argparse.ArgumentParser(description='Get USCSIS naturalization office wait times.')
    parser.add_argument('--form-name', '-f', default='N-400')
    parser.add_argument('--list', '-l',action='store_true', default=False, help='Only list offices')
    parser.add_argument('--key', '-k', action='append', default=[], help='keys to sort by max')
    args = parser.parse_args()

    cwd = os.getcwd()
    if args.list:
        dirs = [cwd]
    else:
        dirs = [d for d in sorted(os.listdir(cwd))
                if os.path.exists(os.path.join(d, args.form_name))]

    all_offices = {}
    for d in dirs:
        formoffices = json_load_file(os.path.join(d, args.form_name))
        all_offices.update({office['office_code'] : office for office in extract_offices(formoffices)})
    output = list(all_offices.values())

    if args.list:
        for office in output:
            print("{office_code}".format(**office))
    else:
        for directory in dirs:
            add_times(output, directory)
        output.sort(key=lambda office: [max(office[key]) for key in args.key])
        for office in output:
            print("{office_description} ({office_code}): {lower_time} - {upper_time}".format(**office))


if __name__ == '__main__':
    main()
